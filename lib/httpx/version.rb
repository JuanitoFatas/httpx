# frozen_string_literal: true

module HTTPX
  VERSION = "0.6.6"
end
